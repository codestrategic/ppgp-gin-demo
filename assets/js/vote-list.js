// noinspection JSJQueryEfficiency,ES6ConvertVarToLetConst

jq2 = jQuery.noConflict();
jq2(function ($) {
    var minDate = $(".datepicker").data("mindate");
    var maxDate = $(".datepicker").data("maxdate");
    $(".datepicker").datepicker({
        "autoclose": true,
        "startDate": minDate,
        "endDate": maxDate
    });
})
