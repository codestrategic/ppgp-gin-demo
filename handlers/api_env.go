package handlers

import (
	"codestrategic.com/ppgp/database"
	"github.com/gin-contrib/sessions"
)

type APIEnv struct {
	dbConnection *database.DbConnection
	sessionStore sessions.Store
}

func NewApiEnv(dbConnection *database.DbConnection, sessionStore sessions.Store) *APIEnv {
	return &APIEnv{dbConnection: dbConnection, sessionStore: sessionStore}
}

func (a APIEnv) GetDbConnection() *database.DbConnection {
	return a.dbConnection
}

func (a APIEnv) GetSessionStore() sessions.Store {
	return a.sessionStore
}
