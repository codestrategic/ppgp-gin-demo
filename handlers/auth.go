package handlers

import (
	"codestrategic.com/ppgp/configuration"
	"codestrategic.com/ppgp/database"
	"codestrategic.com/ppgp/services"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/pquerna/otp/totp"
	"net/http"
	"regexp"
	"strings"
	"time"
)

func (a *APIEnv) ShowLoginPage(c *gin.Context) {
	var title = "Login"
	if c.MustGet("is_logged_in").(bool) {
		title = "Successful Login"
	}

	session := sessions.Default(c)
	errorFlashes := session.Flashes("danger")
	successFlashes := session.Flashes("success")
	_ = session.Save()

	services.Render(c, gin.H{
		"title":          title,
		"ErrorFlashes":   errorFlashes,
		"SuccessFlashes": successFlashes,
	}, "login.admin-no-menu.html")
}

func (a *APIEnv) PerformLogin(c *gin.Context) {
	email := c.PostForm("email")
	password := c.PostForm("password")

	users := database.UsersRepository(a.dbConnection)
	if users.IsUserValid(email, password) {
		user, _ := users.GetUserByEmail(email)
		c.Set("user_id", user.ID)
		c.Set("is_logged_in", true)
		c.Set("user_trusted_version", user.GetTrustedVersionHash())
		session := sessions.Default(c)
		session.Set("is_logged_in", true)
		session.Set("user_id", user.ID)
		session.Set("is_2fa_enabled", user.TwoFaSecret != "")
		session.Set("is_2fa_passed", false)
		session.Set("user_trusted_version", user.GetTrustedVersionHash())

		originalRequestUri, ok := session.Get("original_request_uri").(string)
		if ok && originalRequestUri != "" && user.TwoFaSecret == "" {
			session.Set("original_request_uri", nil)
		}

		_ = session.Save()

		if originalRequestUri == "" {
			c.Redirect(http.StatusFound, "/admin")
		} else {
			c.Redirect(http.StatusFound, originalRequestUri)
		}

	} else {

		c.HTML(http.StatusBadRequest, "login.admin-no-menu.html", gin.H{
			"ErrorTitle":   "Login Failed",
			"ErrorMessage": "Invalid credentials provided",
		})
	}
}

func (a *APIEnv) Show2faValidationPage(c *gin.Context) {
	var title = "Validate"
	if c.GetBool("is_2fa_passed") {
		title = "Successful Validation"
	}

	session := sessions.Default(c)
	errorFlashes := session.Flashes("danger")
	successFlashes := session.Flashes("success")
	_ = session.Save()

	services.Render(c, gin.H{
		"title":          title,
		"ErrorFlashes":   errorFlashes,
		"SuccessFlashes": successFlashes,
	}, "login2fa.admin-no-menu.html")

}

func (a *APIEnv) Perform2faValidation(c *gin.Context) {
	session := sessions.Default(c)
	userId, ok := session.Get("user_id").(uint)
	if !ok {
		session.Set("is_2fa_passed", false)
		_ = session.Save()
		c.Set("is_2fa_passed", false)
		c.HTML(http.StatusBadRequest, "login.admin-no-menu.html", gin.H{
			"ErrorTitle":   "Login Failed",
			"ErrorMessage": "User not found",
		})
		return
	}

	users := database.UsersRepository(a.dbConnection)
	user, err := users.GetUserByID(userId)
	if err != nil {
		session.Set("is_2fa_passed", false)
		_ = session.Save()
		c.Set("is_2fa_passed", false)
		c.HTML(http.StatusBadRequest, "login.admin-no-menu.html", gin.H{
			"ErrorTitle":   "Login Failed",
			"ErrorMessage": "User not found",
		})
		return
	}

	verificationCode := c.PostForm("2fa_verification_code")
	if verificationCode == "" {
		session.Set("is_2fa_passed", false)
		session.AddFlash("Verification code must not be empty", "danger")
		_ = session.Save()
		c.Set("is_2fa_passed", false)
		c.Redirect(http.StatusFound, "/auth/validate2fa")
		return
	}

	strippedVerificationCode := strings.ReplaceAll(verificationCode, " ", "")
	valid := totp.Validate(strippedVerificationCode, user.TwoFaSecretEncoded)
	if !valid {
		session.Set("is_2fa_passed", false)
		session.AddFlash("Verification code was not valid", "danger")
		_ = session.Save()
		c.Set("is_2fa_passed", false)
		c.Redirect(http.StatusFound, "/auth/validate2fa")
		return
	}

	session.Set("is_2fa_passed", true)
	_ = session.Save()
	c.Set("is_2fa_passed", true)

	trustThisDevice := c.PostForm("2fa_trust_this_device")
	if trustThisDevice == "1" {
		expireToken := time.Now().Add(time.Hour * 24 * 500).Unix()
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"iss": "ppgp",
			"exp": expireToken,
			"sub": "Trust this device",
			"adr": c.ClientIP(),
			"ver": user.GetTrustedVersionHash(),
		})
		tokenString, err := token.SignedString([]byte(configuration.Config.TwoFa.HmacSecret))
		if err == nil {
			c.SetCookie(
				"ppgp_trusted_device",
				tokenString,
				int((time.Hour * 24 * 500).Seconds()),
				"/",
				c.Request.Host,
				false,
				true,
			)
		}
		session.Set("user_trusted_version", user.GetTrustedVersionHash())
		c.Set("user_trusted_version", user.GetTrustedVersionHash())
		_ = session.Save()
	}

	originalRequestUri, ok := session.Get("original_request_uri").(string)
	if !ok || originalRequestUri == "" {
		c.Redirect(http.StatusFound, "/admin")
	} else {
		session.Set("original_request_uri", nil)
		_ = session.Save()
		c.Redirect(http.StatusFound, originalRequestUri)
	}
}

func (a *APIEnv) PerformRecoveryCodeValidation(c *gin.Context) {
	session := sessions.Default(c)
	session.Set("is_2fa_passed", false)
	_ = session.Save()
	c.Set("is_2fa_passed", false)
	userId, ok := session.Get("user_id").(uint)
	if !ok {
		c.HTML(http.StatusBadRequest, "login.admin-no-menu.html", gin.H{
			"ErrorTitle":   "Login Failed",
			"ErrorMessage": "User not found",
		})
		return
	}

	users := database.UsersRepository(a.dbConnection)
	user, err := users.GetUserByID(userId)
	if err != nil {
		c.HTML(http.StatusBadRequest, "login.admin-no-menu.html", gin.H{
			"ErrorTitle":   "Login Failed",
			"ErrorMessage": "User not found",
		})
		return
	}

	recoveryCode := c.PostForm("recovery_code")
	if recoveryCode == "" {
		session.AddFlash("Recovery code must not be empty", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/auth/validateRecoveryCode")
		return
	}

	re := regexp.MustCompile(`[ \-]`)
	normalizedRecoveryCode := re.ReplaceAllString(recoveryCode, "")
	normalizedRecoveryCode = strings.ToUpper(normalizedRecoveryCode)

	keyString := configuration.Config.TwoFa.EncodingKey
	if user.HasRecoveryCode(normalizedRecoveryCode, keyString) {
		if user, _, err := user.RemoveRecoveryCode(normalizedRecoveryCode, keyString); err == nil {
			if user, err = users.SaveUser(user); err == nil {
				session.Set("is_2fa_passed", true)
				_ = session.Save()
				c.Set("is_2fa_passed", true)
				originalRequestUri, ok := session.Get("original_request_uri").(string)
				if !ok || originalRequestUri == "" {
					c.Redirect(http.StatusFound, "/admin")
				} else {
					session.Set("original_request_uri", nil)
					_ = session.Save()
					c.Redirect(http.StatusFound, originalRequestUri)
				}
				return
			}
		}
	}

	session.AddFlash("Recovery code was not valid", "danger")
	_ = session.Save()
	c.Set("is_2fa_passed", false)
	c.Redirect(http.StatusFound, "/auth/validateRecoveryCode")
}

func (a *APIEnv) ShowRecoveryCodeValidationPage(c *gin.Context) {
	var title = "Log in with Recovery Code"

	session := sessions.Default(c)
	errorFlashes := session.Flashes("danger")
	successFlashes := session.Flashes("success")
	_ = session.Save()

	services.Render(c, gin.H{
		"title":          title,
		"ErrorFlashes":   errorFlashes,
		"SuccessFlashes": successFlashes,
	}, "loginRecoveryCode.admin-no-menu.html")

}

func (a *APIEnv) Logout(c *gin.Context) {
	c.Set("is_logged_in", false)
	c.Set("user_id", nil)
	c.Set("user_trusted_version", nil)
	session := sessions.Default(c)
	session.Set("is_logged_in", false)
	session.Set("user_id", nil)
	session.Set("user_trusted_version", nil)
	_ = session.Save()
	c.Redirect(http.StatusTemporaryRedirect, "/auth/login")
}
