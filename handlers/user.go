package handlers

import (
	"codestrategic.com/ppgp/database"
	"codestrategic.com/ppgp/services"
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func (a *APIEnv) ShowUsers(c *gin.Context) {
	var title = "Users"

	users := database.UsersRepository(a.dbConnection)
	usersList := users.GetAllUsers()

	session := sessions.Default(c)
	errorFlashes := session.Flashes("danger")
	successFlashes := session.Flashes("success")

	services.Render(c, gin.H{
		"title":          title,
		"menuSection":    "users",
		"Users":          usersList,
		"ErrorFlashes":   errorFlashes,
		"SuccessFlashes": successFlashes,
	}, "show_users.admin.html")
}

func (a *APIEnv) ViewUser(c *gin.Context) {
	session := sessions.Default(c)

	strId := c.Param("id")
	id, err := strconv.ParseUint(strId, 10, 32)
	if err != nil {
		session.AddFlash("user ID must be an integer")
	}

	users := database.UsersRepository(a.dbConnection)
	user, err := users.GetUserByID(uint(id))
	if err != nil || user == nil {
		session.AddFlash("did not find a valid user for the ID")
	}

	errorFlashes := session.Flashes("danger")
	successFlashes := session.Flashes("success")
	_ = session.Save()

	services.Render(c, gin.H{
		"title":          "View user",
		"user":           user,
		"ErrorFlashes":   errorFlashes,
		"SuccessFlashes": successFlashes,
		"menuSection":    "users",
	}, "user_view.admin.html")
}

func (a *APIEnv) EditUser(c *gin.Context) {
	session := sessions.Default(c)

	strId := c.Param("id")
	id, err := strconv.ParseUint(strId, 10, 32)
	if err != nil {
		session.AddFlash("user ID must be an integer")
	}

	users := database.UsersRepository(a.dbConnection)
	user, err := users.GetUserByID(uint(id))
	if err != nil || user == nil {
		session.AddFlash("did not find a valid user for the ID")
	}

	errorFlashes := session.Flashes("danger")
	successFlashes := session.Flashes("success")
	_ = session.Save()

	services.Render(c, gin.H{
		"title":            "Edit user",
		"user":             user,
		"userHasUserRole":  user.HasRole("role_user"),
		"userHasAdminRole": user.HasRole("role_admin"),
		"ErrorFlashes":     errorFlashes,
		"SuccessFlashes":   successFlashes,
		"menuSection":      "users",
	}, "user_edit.admin.html")
}

func (a *APIEnv) UpdateUser(c *gin.Context) {
	users := database.UsersRepository(a.dbConnection)
	session := sessions.Default(c)
	userIsValid := true

	strId := c.Param("id")
	userId, err := strconv.ParseUint(strId, 10, 32)
	if err != nil {
		session.AddFlash("user ID must be an integer, changes not saved")
		userIsValid = false
	}
	user, err := users.GetUserByID(uint(userId))
	if err != nil {
		session.AddFlash("user not found for this ID, changes not saved")
		userIsValid = false
	}

	if userIsValid {
		email := c.PostForm("email")
		roleUser := c.PostForm("roleUser")
		roleAdmin := c.PostForm("roleAdmin")
		password := c.PostForm("password")
		repeatPassword := c.PostForm("repeatPassword")
		removeTrustAllDevices := c.PostForm("removeTrustAllDevices")

		user.Email = email

		if password != "" {
			if repeatPassword != password {
				userIsValid = false
				session.AddFlash("Passwords must match, changes not saved", "danger")
			}
		}

		if userIsValid {
			var roles []string
			if roleUser != "" {
				roles = append(roles, "role_user")
			}
			if roleAdmin != "" {
				roles = append(roles, "role_admin")
			}

			user, err = user.SetRoles(roles)
			if err != nil {
				return
			}

			if removeTrustAllDevices == "1" {
				user.IncrementTrustedVersion()
			}

			if _, err = users.SaveUser(user); err == nil {
				session.AddFlash("User was updated", "success")
			} else {
				session.AddFlash(err.Error(), "danger")
			}
		}
	}

	_ = session.Save()

	c.Redirect(http.StatusFound, fmt.Sprintf("/user/edit/%d", user.ID))
}

func (a *APIEnv) EditMyProfile(c *gin.Context) {
	users := database.UsersRepository(a.dbConnection)
	session := sessions.Default(c)
	userId := session.Get("user_id").(uint)

	user, _ := users.GetUserByID(userId)
	errorFlashes := session.Flashes("danger")
	successFlashes := session.Flashes("success")
	_ = session.Save()
	var title = "My Profile"
	services.Render(c, gin.H{
		"title":          title,
		"userEmail":      user.Email,
		"ErrorFlashes":   errorFlashes,
		"SuccessFlashes": successFlashes,
		"menuSection":    "my_profile",
	}, "my_profile.admin.html")
}

func (a *APIEnv) UpdateMyProfile(c *gin.Context) {
	users := database.UsersRepository(a.dbConnection)
	session := sessions.Default(c)
	userId := session.Get("user_id").(uint)
	user, err := users.GetUserByID(userId)
	userIsValid := err == nil

	if userIsValid {
		email := c.PostForm("email")
		password := c.PostForm("password")
		repeatPassword := c.PostForm("repeatPassword")
		removeTrustAllDevices := c.PostForm("removeTrustAllDevices")

		user.Email = email

		if password != "" {
			if repeatPassword != password {
				userIsValid = false
				session.AddFlash("Passwords must match, changes not saved", "danger")
			} else {
				user, err = user.SetPassword(password)
				if err != nil {
					session.AddFlash("Unable to update the password, changes not saved", "danger")
					userIsValid = false
				}
			}
		}

		if userIsValid {
			if removeTrustAllDevices == "1" {
				user = user.IncrementTrustedVersion()
			}
			_, err := users.SaveUser(user)
			if err != nil {
				session.AddFlash("Encountered an error, changes not saved; "+err.Error(), "danger")
			} else {
				session.AddFlash("User was updated", "success")
			}
		}
	} else {
		session.AddFlash("Encountered an error, changes not saved; "+err.Error(), "danger")
	}

	_ = session.Save()

	c.Redirect(http.StatusFound, "/user/my_profile")
}
