package handlers

import (
	"bytes"
	"codestrategic.com/ppgp/configuration"
	"codestrategic.com/ppgp/database"
	"codestrategic.com/ppgp/services"
	"encoding/base32"
	"encoding/base64"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/pquerna/otp/totp"
	"html/template"
	"image/png"
	"net/http"
	"time"
)

func toBase64(b []byte) string {
	return base64.StdEncoding.EncodeToString(b)
}

func (a *APIEnv) Status2fa(c *gin.Context) {
	var title = "2-Factor Authentication Status"

	session := sessions.Default(c)
	session.Set("configuring_2fa", false)
	errorFlashes := session.Flashes("danger")
	successFlashes := session.Flashes("success")
	_ = session.Save()

	users := database.UsersRepository(a.dbConnection)
	userId := session.Get("user_id").(uint)

	user, _ := users.GetUserByID(userId)

	twoFaEnabled := user.TwoFaSecret != ""

	codes := make([]string, 0)

	if twoFaEnabled {
		keyString := configuration.Config.TwoFa.EncodingKey
		codes = user.GetRecoveryCodes(keyString)
		if len(codes) == 0 {
			if user, _, err := user.InitializeRecoverCodes(keyString); err == nil {
				user, _ = users.SaveUser(user)
			}
		}
		codes = user.GetRecoveryCodes(keyString)
	}

	services.Render(c, gin.H{
		"title":          title,
		"menuSection":    "my_profile",
		"ErrorFlashes":   errorFlashes,
		"SuccessFlashes": successFlashes,
		"twoFaEnabled":   twoFaEnabled,
		"recoveryCodes":  codes,
	}, "2fa_status.admin.html")

}

func (a *APIEnv) Enable2fa(c *gin.Context) {

	session := sessions.Default(c)

	session.Set("configuring_2fa", false)

	users := database.UsersRepository(a.dbConnection)
	userId := session.Get("user_id").(uint)

	user, err := users.GetUserByID(userId)
	if err != nil {
		session.AddFlash("Could not find the current user, log out and back in again", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      configuration.Config.TwoFa.Issuer,
		AccountName: user.Email,
	})
	if err != nil {
		session.AddFlash(err.Error(), "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	generatedSecret := key.Secret()
	var b32NoPadding = base32.StdEncoding.WithPadding(base32.NoPadding)
	encodedSecret := b32NoPadding.EncodeToString([]byte(generatedSecret))

	user.SetTwoFaSecret(generatedSecret, encodedSecret)
	user.ClearRecoveryCodes()
	if _, err = users.SaveUser(user); err != nil {
		session.AddFlash(err.Error(), "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	session.AddFlash("2-factor authentication is enabled", "success")
	session.Set("configuring_2fa", true)
	_ = session.Save()

	c.Redirect(http.StatusFound, "/user/2fa/configure")
}

func (a *APIEnv) Configure2fa(c *gin.Context) {
	var title = "Configure 2-Factor Authentication"

	session := sessions.Default(c)

	if !session.Get("configuring_2fa").(bool) {
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	errorFlashes := session.Flashes("danger")
	successFlashes := session.Flashes("success")
	_ = session.Save()

	users := database.UsersRepository(a.dbConnection)
	userId := session.Get("user_id").(uint)

	user, err := users.GetUserByID(userId)
	if err != nil {
		session.AddFlash("Could not find the user; log out and back in again", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	//secret := base32.StdEncoding.EncodeToString([]byte(user.TwoFaSecret))

	key, err := totp.Generate(totp.GenerateOpts{
		Issuer:      configuration.Config.TwoFa.Issuer,
		AccountName: user.Email,
		Secret:      []byte(user.TwoFaSecret),
	})
	if err != nil {
		session.AddFlash("Could not retrieve the account configuration; try again", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	var buf bytes.Buffer
	img, err := key.Image(200, 200)
	if err != nil {
		session.AddFlash("Could not create the bar code; try again", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}
	err = png.Encode(&buf, img)
	if err != nil {
		session.AddFlash("Could not create the bar code; try again", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	samplePasscode, err := totp.GenerateCode(user.TwoFaSecret, time.Now())
	if err != nil {
		session.AddFlash("Could not create the sample passcode; try again", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	samplePasscodeFromEncoded, err := totp.GenerateCode(user.TwoFaSecretEncoded, time.Now())
	if err != nil {
		session.AddFlash("Could not create the sample passcode from encoded; try again", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	var base64Encoding string
	base64Encoding += "data:image/png;base64,"
	base64Encoding += toBase64(buf.Bytes())

	services.Render(c, gin.H{
		"title":           title,
		"menuSection":     "my_profile",
		"ErrorFlashes":    errorFlashes,
		"SuccessFlashes":  successFlashes,
		"EnrollImageText": template.URL(base64Encoding),
		"Issuer":          key.Issuer(),
		"Secret":          key.Secret(),
		"StoredSecret":    user.TwoFaSecret,
		"EncodedSecret":   user.TwoFaSecretEncoded,
		"SamplePasscode":  samplePasscode,
		"EncodedPasscode": samplePasscodeFromEncoded,
	}, "2fa_configure.admin.html")
}

func (a *APIEnv) Confirm2fa(c *gin.Context) {
	session := sessions.Default(c)

	users := database.UsersRepository(a.dbConnection)
	userId := session.Get("user_id").(uint)

	user, err := users.GetUserByID(userId)
	if err != nil {
		session.AddFlash("Could not find the user; log out and back in again", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	verificationCode := c.PostForm("2fa_verification_code")
	if verificationCode == "" {
		session.AddFlash("Verification code must not be empty", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/configure")
		return
	}

	valid := totp.Validate(verificationCode, user.TwoFaSecretEncoded)
	if !valid {
		session.AddFlash("Verification code was not valid; check your passcode application settings", "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/configure")
		return
	}

	session.AddFlash("Passcode was confirmed; 2-factor authentication is properly configured", "success")
	_ = session.Save()
	c.Redirect(http.StatusFound, "/user/2fa/status")
}

func (a *APIEnv) Disable2fa(c *gin.Context) {

	session := sessions.Default(c)

	users := database.UsersRepository(a.dbConnection)
	userId := session.Get("user_id").(uint)

	user, err := users.GetUserByID(userId)
	if err != nil {
		session.AddFlash(err.Error(), "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	user.SetTwoFaSecret("", "")
	user.ClearRecoveryCodes()
	if user, err = users.SaveUser(user); err != nil {
		session.AddFlash(err.Error(), "danger")
		_ = session.Save()
		c.Redirect(http.StatusFound, "/user/2fa/status")
		return
	}

	session.AddFlash("2-factor authentication is disabled", "success")
	_ = session.Save()
	c.Redirect(http.StatusFound, "/user/2fa/status")
}
