package handlers

import (
	"codestrategic.com/ppgp/configuration"
	"codestrategic.com/ppgp/models"
	"codestrategic.com/ppgp/services"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"os"
	"time"
)

func (a *APIEnv) ShowHomePage(c *gin.Context) {

	var goalData models.GoalData

	dat, err := os.ReadFile(configuration.Config.Server.ApplicationBaseDirectory + "/assets/data/goal_status.json")
	if err == nil {
		err := json.Unmarshal(dat, &goalData)
		if err != nil {
			goalData.GoalCount = -1
			goalData.RetrievalDate = time.Now()
		}
	} else {
		goalData.GoalCount = -1
		goalData.RetrievalDate = time.Now()
	}
	services.Render(c, gin.H{
		"title":    "Home Page",
		"goalData": goalData,
	}, "home.public.html")
}
