package configuration

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
)

type Configurations struct {
	Server           ServerConfigurations
	Database         DatabaseConfigurations
	PasswordHashCost int
	Smtp             SmtpConfigurations
	TwoFa            TwoFaConfigurations
	Redis            RedisConfigurations
}

type ServerConfigurations struct {
	Port                     int
	ApplicationBaseDirectory string
}

type DatabaseConfigurations struct {
	DBName string
	Dsn    string
	DbType string
}

type SmtpConfigurations struct {
	Server   string
	Port     int
	Username string
	Password string
}

type TwoFaConfigurations struct {
	Issuer      string
	HmacSecret  string
	EncodingKey string
}

type RedisConfigurations struct {
	Size          int
	Network       string
	Address       string
	Password      string
	KeyPairString string
}

var Config *Configurations

func NewConfiguration(env *string) *Configurations {
	viper.SetConfigFile("./conf/.env")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Printf("Did not find .env file")
	}

	if env == nil || *env == "" {
		var newEnv = "dev"
		appEnv := viper.Get("APP_ENV")
		if appEnv != nil {
			newEnv = appEnv.(string)
		}
		env = &newEnv
	}

	if *env == "prod" {
		gin.SetMode(gin.ReleaseMode)
	}

	viper.SetConfigName("./conf/config_" + *env + ".yml")
	viper.AddConfigPath(".")
	viper.AddConfigPath("./..")
	viper.AutomaticEnv()
	viper.SetConfigType("yml")
	var config Configurations
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Error reading configuration file, %s", err)
	}
	viper.SetDefault("database.dbname", "data")
	viper.SetDefault("server.port", 8080)
	viper.SetDefault("server.applicationbasedirectory", ".")
	viper.SetDefault("smtp.port", "587")
	err = viper.Unmarshal(&config)
	if err != nil {
		fmt.Printf("Unable to decode into struct, %v", err)
	}
	return &config
}
