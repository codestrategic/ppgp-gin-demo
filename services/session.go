package services

import (
	"codestrategic.com/ppgp/configuration"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/redis"
)

func InitSession(config *configuration.Configurations) (sessions.Store, error) {

	store, err := redis.NewStore(config.Redis.Size,
		config.Redis.Network,
		config.Redis.Address,
		config.Redis.Password,
		[]byte(config.Redis.KeyPairString))
	return store, err
}
