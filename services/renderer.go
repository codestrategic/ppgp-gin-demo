package services

import (
	"github.com/gin-contrib/multitemplate"
	"github.com/gin-gonic/gin"
	"net/http"
	"path/filepath"
	"strings"
)

func Render(c *gin.Context, data gin.H, templateName string) {

	switch c.Request.Header.Get("Accept") {
	case "application/json":
		c.JSON(http.StatusOK, data["payload"])
	case "application/xml":
		c.XML(http.StatusOK, data["payload"])
	default:
		data["is_logged_in"] = c.MustGet("is_logged_in").(bool)
		c.HTML(http.StatusOK, templateName, data)
	}

}

func LoadTemplates(templatesDir string) multitemplate.Renderer {
	r := multitemplate.NewRenderer()

	layouts, err := filepath.Glob(templatesDir + "/layouts/*.html")
	if err != nil {
		panic(err.Error())
	}

	for _, layout := range layouts {
		layoutFileName := filepath.Base(layout)
		s := strings.Split(layoutFileName, ".")
		layoutName := s[0]

		includes, err := filepath.Glob(templatesDir + "/includes/*." + layoutName + ".html")
		if err != nil {
			panic(err.Error())
		}

		partials, err := filepath.Glob(templatesDir + "/partials/*" + layoutName + ".html")
		if err != nil {
			panic(err.Error())
		}

		funcMap := FormatterGetFuncMap()

		for _, include := range includes {
			files := make([]string, 0)
			files = append(files, layout)
			files = append(files, partials...)
			files = append(files, include)
			r.AddFromFilesFuncs(filepath.Base(include), funcMap, files...)
		}

	}

	//includes, err := filepath.Glob(templatesDir + "/includes/*.html")
	//if err != nil {
	//	panic(err.Error())
	//}
	//
	//partials, err := filepath.Glob(templatesDir + "/partials/*.html")
	//if err != nil {
	//	panic(err.Error())
	//}
	//
	//for _, include := range includes {
	//	layoutCopy := make([]string, len(layouts))
	//	copy(layoutCopy, layouts)
	//	partialCopy := make([]string, len(partials))
	//	copy(partialCopy, partials)
	//	files := append(append(layoutCopy, partialCopy...), include)
	//	r.AddFromFiles(filepath.Base(include), files...)
	//}

	return r
}
