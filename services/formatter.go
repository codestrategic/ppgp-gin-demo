package services

import (
	"fmt"
	"html/template"
)

func FormatterGetFuncMap() template.FuncMap {
	return template.FuncMap{
		"asRecoveryCode": FormatAsRecoveryCode,
	}
}

func FormatAsRecoveryCode(originalCode string) string {
	return fmt.Sprintf("%s-%s", originalCode[:5], originalCode[5:])
}
