package main

import (
	"codestrategic.com/ppgp/configuration"
	"fmt"
	"github.com/jordan-wright/email"
	"net/smtp"
	"strconv"
)

func main() {

	var appEnv = "dev"

	config := configuration.NewConfiguration(&appEnv)

	e := email.NewEmail()
	e.From = "david.a.peoples@gmail.com"
	e.To = []string{"david.a.peoples@gmail.com"}
	e.Subject = "Test email from the ppgp project"
	e.Text = []byte("Test plain-text content from the ppgp project.")
	e.HTML = []byte("<h1>Test</h1><p>Test HTML content from the ppgp project.</p>")
	addr := config.Smtp.Server + ":" + strconv.Itoa(config.Smtp.Port)
	auth := smtp.PlainAuth("", config.Smtp.Username, config.Smtp.Password, config.Smtp.Server)
	err := e.Send(addr, auth)
	if err != nil {
		fmt.Println("Error trying to send an email: " + err.Error())
	} else {
		fmt.Println("Gmail SMTP server accepted the email transmission, test appears to have succeeded.")
	}
}
