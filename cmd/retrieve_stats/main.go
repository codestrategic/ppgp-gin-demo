package main

import (
	"codestrategic.com/ppgp/configuration"
	"codestrategic.com/ppgp/models"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {

	type split struct {
		Stat map[string]interface{}
		Team struct {
			Name string
		}
	}

	type teamStat struct {
		Splits []split
	}

	type team struct {
		Abbreviation string
		Division     struct {
			Id int
		}
		TeamStats []teamStat
	}

	type result struct {
		Copyright string
		Teams     []team
	}

	var Result result

	var appEnv = "dev"

	config := configuration.NewConfiguration(&appEnv)

	url := "https://statsapi.web.nhl.com/api/v1/teams?expand=team.stats"

	apiClient := http.Client{
		Timeout: time.Second * 4,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatalf("NewRequest failed: %v\n", err)
	}

	req.Header.Set("User-Agent", "ppgp-stats-retriever")

	res, getErr := apiClient.Do(req)
	if getErr != nil {
		log.Fatalf("apiClient.Do failed: %v\n", getErr)
	}

	if res.Body != nil {
		defer func(Body io.ReadCloser) {
			_ = Body.Close()
		}(res.Body)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatalf("ReadAll failed: %v\n", readErr)
	}

	if jsonErr := json.Unmarshal(body, &Result); jsonErr != nil {
		log.Fatalf("JSON Unmarshal failed: %v\n", jsonErr)
	}

	var ttlPPG int

	for _, team := range Result.Teams {
		if ppg, ok := team.TeamStats[0].Splits[0].Stat["powerPlayGoals"].(float64); ok {
			ttlPPG += int(ppg)
		}
		//fmt.Printf("%s\t%4d\n", team.Abbreviation, ppg)
	}
	//fmt.Printf("%s\t%[1]s\n", "====")
	//fmt.Printf("%s\t%4d\n", "Ttl", ttlPPG)

	goalData := models.GoalData{
		GoalCount:     ttlPPG,
		RetrievalDate: time.Now(),
	}

	dat, err := json.Marshal(goalData)
	if err != nil {
		log.Fatalf("Marshalling the goalData failed: %v\n", err)
	}
	fmt.Printf("goalData: %s\n", dat)

	err = os.WriteFile(config.Server.ApplicationBaseDirectory+"/assets/data/goal_status.json", dat, 0644)
	if err != nil {
		log.Fatalf("Error writing file: %v\n", err)
	}
}
