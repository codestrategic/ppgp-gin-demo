package main

import (
	"codestrategic.com/ppgp/configuration"
	"codestrategic.com/ppgp/database"
	"encoding/json"
	"fmt"
	"log"
	"os"
)

func main() {

	config := configuration.NewConfiguration(nil)

	dbConnection := database.NewDbConnection(config)

	if len(os.Args) != 4 {
		log.Fatal("usage: create_user [email] [password] [roles]")
	}

	email := os.Args[1]
	password := os.Args[2]
	roles := os.Args[3]
	var goRoles []string
	err := json.Unmarshal([]byte(roles), &goRoles)
	if err != nil {
		log.Fatal("roles must be an array encoded as a JSON string")
	}

	repo := database.UsersRepository(dbConnection)
	user, err := repo.CreateUser(email, password, goRoles)
	if err != nil {
		log.Fatalf("create user failed: %s", err.Error())
	}
	fmt.Printf("Created new user with ID %d\n", user.ID)
}
