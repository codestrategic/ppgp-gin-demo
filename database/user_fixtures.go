package database

import (
	"codestrategic.com/ppgp/models"
	"gorm.io/datatypes"
)

var UserFixtures = []models.User{
	{Email: "user1", Password: "pass1", Roles: datatypes.JSON(`{middleware.RoleUser, middleware.RoleAdmin}`)},
	{Email: "user2", Password: "pass2", Roles: datatypes.JSON(`{middleware.RoleUser}`)},
	{Email: "user3", Password: "pass3", Roles: datatypes.JSON(`{middleware.RoleUser}`)},
}
