package database

import (
	"codestrategic.com/ppgp/models"
	"encoding/json"
	"errors"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/datatypes"
	"strings"
)

type usersRepository struct {
	dbConnection *DbConnection
}

func UsersRepository(dbConnection *DbConnection) *usersRepository {
	return &usersRepository{dbConnection: dbConnection}
}

func (u *usersRepository) CreateUser(email, password string, roles []string) (*models.User, error) {
	if strings.TrimSpace(password) == "" {
		return nil, errors.New("the password cannot be empty")
	} else if !u.IsEmailAvailable(email) {
		return nil, errors.New("the email address is already in use by another account")
	}

	rolesAsJSON, err := json.Marshal(roles)
	if err != nil {
		return nil, errors.New("the submitted roles could not be converted to a JSON string")
	}

	user := &models.User{
		Email:    email,
		Password: u.InlineHashPassword(password),
		Roles:    datatypes.JSON(rolesAsJSON),
	}
	err = u.dbConnection.db.Create(user).Error
	return user, err
}

func (u *usersRepository) SaveUser(user *models.User) (*models.User, error) {
	err := u.dbConnection.db.Save(user).Error
	return user, err
}

//func (u *usersRepository) UpdateUser(id uint, email, password string, roles []string) (*models.User, error) {
//	user, err := u.GetUserByID(id)
//	if err != nil {
//		return nil, err
//	}
//	if user == nil {
//		return nil, errors.New("no user found for this ID")
//	}
//
//	// Check for collision with an existing other user
//	otherUser, err := u.GetUserByEmail(email)
//	if err != nil {
//		return nil, err
//	}
//	if otherUser != nil && otherUser.ID != user.ID {
//		return nil, errors.New("the submitted email is already used by another account")
//	}
//	user.Email = email
//
//	if strings.TrimSpace(password) != "" {
//		user.Password = u.InlineHashPassword(password)
//	}
//
//	rolesAsJSON, err := json.Marshal(roles)
//	if err != nil {
//		return nil, errors.New("the submitted roles could not be converted to a JSON string")
//	}
//	user.Roles = rolesAsJSON
//
//	err = u.dbConnection.db.Save(user).Error
//	return user, err
//}

//func (u *usersRepository) UpdateTwoFaSecret(id uint, secret, encodedSecret string) (*models.User, error) {
//	user, err := u.GetUserByID(id)
//	if err != nil {
//		return nil, err
//	}
//	if user == nil {
//		return nil, errors.New("no user found for this ID")
//	}
//
//	user.TwoFaSecret = secret
//	user.TwoFaSecretEncoded = encodedSecret
//	user.IncrementTrustedVersion()
//
//	err = u.dbConnection.db.Save(user).Error
//	return user, err
//}

//func (u *usersRepository) UpdateRecoveryCodes(id uint, codes []string, keyString string) (*models.User, error) {
//	user, err := u.GetUserByID(id)
//	if err != nil {
//		return nil, err
//	}
//	if user == nil {
//		return nil, errors.New("no user found for this ID")
//	}
//
//	_, err = user.SetRecoveryCodes(codes, keyString)
//	if err != nil {
//		return nil, err
//	}
//
//	err = u.dbConnection.db.Save(user).Error
//	if err != nil {
//		return nil, err
//	}
//
//	user, err = u.GetUserByID(id)
//	return user, err
//}

//// IncrementTrustedVersion  Increase the value of user.trustedVersion by 1.
////
//// This has the effect of removing trust from all previously trusted devices,
//// forcing 2fa authentication on all of them for their next logins.
//func (u *usersRepository) IncrementTrustedVersion(id uint) (*models.User, error) {
//	user, err := u.GetUserByID(id)
//	if err != nil {
//		return user, err
//	}
//	if user == nil {
//		return user, errors.New("no user found for this ID")
//	}
//
//	user = user.IncrementTrustedVersion()
//	err = u.dbConnection.db.Save(user).Error
//
//	return user, err
//}

func (u *usersRepository) GetUserByID(id uint) (*models.User, error) {
	var user models.User
	err := u.dbConnection.db.Where("ID = ?", id).First(&user).Error
	return &user, err
}

func (u *usersRepository) GetUserByEmail(email string) (*models.User, error) {
	var user models.User
	err := u.dbConnection.db.Where("email = ?", email).First(&user).Error
	return &user, err
}

func (u *usersRepository) IsEmailAvailable(email string) bool {
	user, err := u.GetUserByEmail(email)
	if err != nil && err.Error() == "record not found" {
		return true
	}
	if err == nil && &user == nil {
		return true
	}
	return false
}

func (u *usersRepository) IsUserValid(email, password string) bool {
	user, err := u.GetUserByEmail(email)

	if err == nil && &user != nil {
		passwordCheckErr := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
		if passwordCheckErr == nil {
			return true
		}
	}
	return false
}

//goland:noinspection GoUnusedExportedFunction
func (u *usersRepository) GetAllUsers() []models.User {
	var users []models.User
	u.dbConnection.db.Find(&users)
	return users
}

//goland:noinspection GoUnusedExportedFunction
func (u *usersRepository) RemoveUser(email string) {
	user, err := u.GetUserByEmail(email)
	if err == nil && &user != nil {
		u.dbConnection.db.Delete(user)
	}
}

func (u *usersRepository) HashPassword(password string) (string, error) {
	cost := u.dbConnection.config.PasswordHashCost
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	return string(bytes), err
}

func (u *usersRepository) InlineHashPassword(password string) string {
	hashed, _ := u.HashPassword(password)
	return hashed
}
