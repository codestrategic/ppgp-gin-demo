package database

import (
	"codestrategic.com/ppgp/configuration"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUsernameAvailability(t *testing.T) {
	a := assert.New(t)

	env := "test"
	config := configuration.NewConfiguration(&env)
	dbConnection := NewDbConnection(config)
	dbConnection.LoadUserFixtures()
	users := UsersRepository(dbConnection)
	defer func(db *gorm.DB) {
		err := db.Close()
		if err != nil {
			a.Error(err)
		}
	}(dbConnection.GetDB())

	if !users.IsEmailAvailable("newuser") {
		a.Fail("IsEmailAvailable should have returned true on 'newuser'")
	}

	if users.IsEmailAvailable("user1") {
		a.Fail("IsEmailAvailable should have returned false on 'user1'")
	}

	_, _ = users.CreateUser("newuser", "newpass", []string{"role_user", "role_admin"})

	if users.IsEmailAvailable("newuser") {
		a.Fail("IsEmailAvailable show have returned false on 'newuser' after that account was created")
	}

	dbConnection.ClearUsersTable()
}

// Test that a GET request to the home page returns the home page with
// the HTTP code 200 for an unauthenticated user
func TestValidUserRegistration(t *testing.T) {
	a := assert.New(t)

	env := "test"

	config := configuration.NewConfiguration(&env)
	dbConnection := NewDbConnection(config)
	dbConnection.LoadUserFixtures()
	users := UsersRepository(dbConnection)
	defer func(db *gorm.DB) {
		err := db.Close()
		if err != nil {
			a.Error(err)
		}
	}(dbConnection.GetDB())

	u, err := users.CreateUser("newuser", "newpass", []string{"role_user", "role_admin"})

	a.Nil(err, "err should have been nil")
	a.NotEqual("", u.Email, "Email should not have been the empty string")

	dbConnection.ClearUsersTable()
}

func TestUserValidity(t *testing.T) {
	a := assert.New(t)

	env := "test"

	config := configuration.NewConfiguration(&env)
	dbConnection := NewDbConnection(config)
	dbConnection.LoadUserFixtures()
	users := UsersRepository(dbConnection)
	defer func(db *gorm.DB) {
		err := db.Close()
		if err != nil {
			a.Error(err)
		}
	}(dbConnection.GetDB())

	if !users.IsUserValid("user1", "pass1") {
		a.Fail("User user1/pass1 should have been valid")
	}

	if users.IsUserValid("user2", "pass1") {
		a.Fail("user 'user1' with wrong password should not have been counted as valid")
	}

	if users.IsUserValid("user1", "") {
		a.Fail("user 'user1' with empty password should not have been counted as valid")
	}

	if users.IsUserValid("", "pass1") {
		a.Fail("user with valid password but empty user should not have been counted as valid")
	}

	if users.IsUserValid("User1", "pass1") {
		a.Fail("validity check is not checking for case match on Username")
	}

	dbConnection.ClearUsersTable()
}
