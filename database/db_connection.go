package database

import (
	"codestrategic.com/ppgp/configuration"
	"codestrategic.com/ppgp/models"
	"encoding/json"
	"fmt"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type DbConnection struct {
	db     *gorm.DB
	config *configuration.Configurations
}

func NewDbConnection(config *configuration.Configurations) *DbConnection {
	db := setup(config)
	return &DbConnection{db: db, config: config}
}

func setup(config *configuration.Configurations) *gorm.DB {

	var database *gorm.DB
	var err error
	if config.Database.DbType == "sqlite3" {
		database, err = gorm.Open(sqlite.Open(config.Database.DBName), &gorm.Config{})
	} else {
		database, err = gorm.Open(mysql.Open(config.Database.Dsn), &gorm.Config{})
	}

	if err != nil {
		panic(fmt.Sprintf("Failed to connect to the database! Error: %s", err))
	}

	err = database.AutoMigrate(&models.User{}, &models.GoalData{})
	if err != nil {
		panic(fmt.Sprintf("Failed to automigrate the database! Error: %s", err))
	}

	return database

}

//goland:noinspection GoUnusedExportedFunction
func (c *DbConnection) GetDB() *gorm.DB {
	return c.db
}

func (c *DbConnection) GetConfig() *configuration.Configurations {
	return c.config
}

//goland:noinspection GoUnusedExportedFunction
func (c *DbConnection) ClearAllTables() {
	c.ClearArticlesTable()
	c.ClearUsersTable()
}

func (c *DbConnection) ClearArticlesTable() {
	c.db.Exec("DELETE FROM articles")
	c.db.Exec("UPDATE `sqlite_sequence` SET `seq` = 0 WHERE `name` = 'articles'")
}

func (c *DbConnection) ClearUsersTable() {
	c.db.Exec("DELETE FROM users")
	c.db.Exec("UPDATE `sqlite_sequence` SET `seq` = 0 WHERE `name` = 'users'")
}

func (c *DbConnection) LoadUserFixtures() {
	c.ClearUsersTable()
	users := UsersRepository(c)
	var roles []string
	for _, u := range UserFixtures {
		jsonErr := json.Unmarshal(u.Roles, &roles)
		if jsonErr != nil {
			panic("could not convert roles JSON to []string")
		}
		_, err := users.CreateUser(u.Email, u.Password, roles)
		if err != nil {
			panic("could not create a user fixture")
		}
	}
}
