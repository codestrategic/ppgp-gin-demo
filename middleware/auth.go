package middleware

import (
	"codestrategic.com/ppgp/configuration"
	"fmt"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"net/http"
	"time"
)

//goland:noinspection GoSnakeCaseUsage
const (
	RoleUser  = "role_user"
	RoleAdmin = "role_admin"
)

func IsValidRole(role string) bool {
	if role == RoleUser {
		return true
	}
	if role == RoleAdmin {
		return true
	}
	return false
}

func EnsureLoggedIn() gin.HandlerFunc {
	return func(c *gin.Context) {
		if !c.GetBool("is_logged_in") {
			session := sessions.Default(c)
			session.Set("original_request_uri", c.Request.RequestURI)
			_ = session.Save()
			c.Redirect(http.StatusTemporaryRedirect, "/auth/login")
			c.AbortWithStatus(http.StatusUnauthorized)
		} else {
			if c.GetBool("is_2fa_enabled") {
				if !c.GetBool("is_2fa_passed") {
					c.Redirect(http.StatusTemporaryRedirect, "/auth/validate2fa")
					c.AbortWithStatus(http.StatusUnauthorized)
				}
			}
			session := sessions.Default(c)
			session.Set("original_request_uri", nil)
			_ = session.Save()
		}
	}
}

func EnsureLoginFormSucceeded() gin.HandlerFunc {
	return func(c *gin.Context) {
		if !c.GetBool("is_logged_in") {
			session := sessions.Default(c)
			session.Set("original_request_uri", c.Request.RequestURI)
			_ = session.Save()
			c.Redirect(http.StatusTemporaryRedirect, "/auth/login")
			c.AbortWithStatus(http.StatusUnauthorized)
		}
	}
}

func EnsureNotLoggedIn() gin.HandlerFunc {
	return func(c *gin.Context) {
		if c.GetBool("is_logged_in") {
			c.AbortWithStatus(http.StatusUnauthorized)
		}
	}
}

func SetUserStatus() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		setLoggedInStatusVars(c, session)
		setTwoFaStatusVars(c, session)
		_ = session.Save()
	}
}

func setTwoFaStatusVars(c *gin.Context, session sessions.Session) {
	if twoFaEnabled, ok := session.Get("is_2fa_enabled").(bool); ok {
		session.Set("is_2fa_enabled", twoFaEnabled)
		c.Set("is_2fa_enabled", twoFaEnabled)
		setTwoFaPassedStatusVars(c, twoFaEnabled, session)
	} else {
		clearTwoFaStatusVars(c, session)
		clearTwoFaPassedStatusVars(c, session)
	}
}

func setTwoFaPassedStatusVars(c *gin.Context, twoFaEnabled bool, session sessions.Session) {
	if twoFaEnabled {
		if twoFaPassed, ok := session.Get("is_2fa_passed").(bool); ok {
			if !twoFaPassed {
				twoFaPassed = testTrustedDeviceCookie(c)
			}
			session.Set("is_2fa_passed", twoFaPassed)
			c.Set("is_2fa_passed", twoFaPassed)
		} else {
			clearTwoFaPassedStatusVars(c, session)
		}
	}
}

func clearTwoFaStatusVars(c *gin.Context, session sessions.Session) {
	session.Set("is_2fa_enabled", false)
	c.Set("is_2fa_enabled", false)
}

func clearTwoFaPassedStatusVars(c *gin.Context, session sessions.Session) {
	session.Set("is_2fa_passed", false)
	c.Set("is_2fa_passed", false)
}

func setLoggedInStatusVars(c *gin.Context, session sessions.Session) {
	if loggedIn, ok := session.Get("is_logged_in").(bool); ok {
		session.Set("is_logged_in", loggedIn)
		c.Set("is_logged_in", loggedIn)
	} else {
		session.Set("is_logged_in", false)
		c.Set("is_logged_in", false)
	}
}

func cookieHasNotExpired(claims jwt.MapClaims) bool {
	exp, _ := claims["exp"].(float64)
	expDate := time.Unix(int64(exp), 0)
	now := time.Now()
	return now.Before(expDate)
}

func ipAddressesMatch(claims jwt.MapClaims, c *gin.Context) bool {
	adr, _ := claims["adr"].(string)
	return adr == c.ClientIP()
}

func versionsMatch(claims jwt.MapClaims, c *gin.Context) bool {
	ver, _ := claims["ver"].(string)
	session := sessions.Default(c)
	if storedTrustedVersion, ok := session.Get("user_trusted_version").(string); ok {
		return storedTrustedVersion == ver
	}
	return false
}

func testTrustedDeviceCookie(c *gin.Context) bool {
	tokenString, err := c.Cookie("ppgp_trusted_device")
	if err != nil {
		return false
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(configuration.Config.TwoFa.HmacSecret), nil
	})
	if err != nil {
		return false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if cookieHasNotExpired(claims) && ipAddressesMatch(claims, c) && versionsMatch(claims, c) {
			return true
		}
	}

	return false
}
