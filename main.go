package main

import (
	"codestrategic.com/ppgp/configuration"
	"codestrategic.com/ppgp/database"
	"codestrategic.com/ppgp/handlers"
	"codestrategic.com/ppgp/services"
	"fmt"
	"github.com/fvbock/endless"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"html/template"
	"log"
	"os"
	"syscall"
)

var router *gin.Engine

func main() {

	configuration.Config = configuration.NewConfiguration(nil)

	db := database.NewDbConnection(configuration.Config)

	store, _ := services.InitSession(configuration.Config)
	api := handlers.NewApiEnv(db, store)

	router = gin.Default()

	_ = router.SetTrustedProxies(nil)

	router.SetFuncMap(template.FuncMap{
		"asRecoveryCode": services.FormatAsRecoveryCode,
	})

	router.Use(sessions.Sessions("gin_session", store))

	router.HTMLRender = services.LoadTemplates("./templates")

	InitializeRoutes(api)

	//models.ConnectDatabase()

	//// Start serving the application
	//err := router.Run(fmt.Sprintf("localhost:%d", configuration.Config.Server.Port))
	//if err != nil {
	//	return
	//}

	server := endless.NewServer(fmt.Sprintf(":%d", configuration.Config.Server.Port), router)
	server.BeforeBegin = func(add string) {
		log.Printf("ppgp-app | starting server on %s\n", add)
		log.Printf("ppgp-app | pid is %d\n", syscall.Getpid())
	}

	err := server.ListenAndServe()
	if err != nil {
		log.Printf("ppgp-app | error: %s\n", err)
	}

	log.Printf("ppgp-app | server on %d stopped\n", configuration.Config.Server.Port)

	os.Exit(0)
}
