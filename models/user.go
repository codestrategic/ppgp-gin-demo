package models

import (
	"codestrategic.com/ppgp/configuration"
	"codestrategic.com/ppgp/middleware"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	b64 "encoding/base64"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/datatypes"
	"io"
	"log"
	"math/big"
	"strconv"
)

type User struct {
	gorm.Model
	Email              string `json:"email"`
	Password           string `json:"-"`
	Roles              datatypes.JSON
	TwoFaSecret        string `json:"-"`
	TwoFaSecretEncoded string `json:"-"`
	TrustedVersion     uint   `json:"-" gorm:"->:false;<-"`
	RecoveryCodes      string `json:"-" gorm:"type:text"`
}

func (u *User) SetPassword(password string) (*User, error) {
	hashedPassword, err := u.hashPassword(password)
	if err == nil {
		u.Password = hashedPassword
		return u, nil
	}
	return u, err
}

func (u *User) SetTwoFaSecret(secret, encodedSecret string) *User {
	u.TwoFaSecret = secret
	u.TwoFaSecretEncoded = encodedSecret
	u.IncrementTrustedVersion()
	return u
}

func (u *User) GetRoles() []string {
	var roles []string
	err := json.Unmarshal(u.Roles, &roles)
	if err != nil {
		log.Fatalf("Could not hydrate User.Roles: %s", err.Error())
	}
	return roles
}

func (u *User) SetRoles(roles []string) (*User, error) {
	for _, a := range roles {
		if !middleware.IsValidRole(a) {
			return nil, fmt.Errorf("role %s is not a valid role", a)
		}
	}

	data, err := json.Marshal(&roles)
	if err != nil {
		return nil, err
	}
	u.Roles = data
	return u, nil
}

func (u *User) HasRole(role string) bool {
	roles := u.GetRoles()
	for _, a := range roles {
		if a == role {
			return true
		}
	}
	return false
}

func (u *User) GetRecoveryCodes(keyString string) []string {
	var codes []string
	if u.RecoveryCodes == "" {
		return make([]string, 0)
	}
	recoveryCodesText := u.decryptCodes(u.RecoveryCodes, keyString)
	if err := json.Unmarshal(recoveryCodesText, &codes); err != nil {
		log.Fatalf("Could not hydrate User.RecoveryCodes: #{err.Error()}")
	}
	return codes
}

func (u *User) InitializeRecoverCodes(keyString string) (*User, []string, error) {
	codes, err := u.generateCodes()
	if err != nil {
		return u, codes, err
	}
	return u.SetRecoveryCodes(codes, keyString)
}

func (u *User) SetRecoveryCodes(codes []string, keyString string) (*User, []string, error) {
	if data, err := json.Marshal(&codes); err == nil {
		u.RecoveryCodes = u.encryptCodes(data, keyString)
		return u, codes, nil
	} else {
		return u, codes, err
	}
}

func (u *User) ClearRecoveryCodes() *User {
	u.RecoveryCodes = ""
	return u
}

func (u *User) HasRecoveryCode(code string, keyString string) bool {
	codes := u.GetRecoveryCodes(keyString)
	for _, a := range codes {
		if a == code {
			return true
		}
	}
	return false
}

func (u *User) RemoveRecoveryCode(code, keyString string) (*User, []string, error) {
	existingCodes := u.GetRecoveryCodes(keyString)
	var unusedCodes []string
	for _, a := range existingCodes {
		if a != code {
			unusedCodes = append(unusedCodes, a)
		}
	}
	_, codes, err := u.SetRecoveryCodes(unusedCodes, keyString)
	return u, codes, err
}

func (u *User) GetTrustedVersionHash() string {
	result := md5.Sum([]byte(strconv.FormatUint(uint64(u.TrustedVersion), 10) + "|" + u.TwoFaSecret))
	return b64.StdEncoding.EncodeToString(result[:])
}

func (u *User) IncrementTrustedVersion() *User {
	u.TrustedVersion = u.TrustedVersion + 1
	return u
}

func (u *User) encryptCodes(decryptedCodes []byte, keyString string) string {
	key, _ := hex.DecodeString(keyString)
	block, err := aes.NewCipher(key)
	if err != nil {
		return ""
	}
	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return ""
	}
	nonce := make([]byte, aesGCM.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return ""
	}
	ciphertext := aesGCM.Seal(nonce, nonce, decryptedCodes, nil)
	return fmt.Sprintf("%x", ciphertext)
}

func (u *User) decryptCodes(encryptedCodesText, keyString string) []byte {
	key, _ := hex.DecodeString(keyString)
	enc, _ := hex.DecodeString(encryptedCodesText)
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil
	}
	aesGCM, err := cipher.NewGCM(block)
	if err != nil {
		return nil
	}
	nonceSize := aesGCM.NonceSize()
	nonce, ciphertext := enc[:nonceSize], enc[nonceSize:]
	plaintext, err := aesGCM.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil
	}
	return plaintext
}

func (u *User) generateCodes() ([]string, error) {
	var codes = make([]string, 0, 16)
	for i := 0; i < 16; i++ {
		code, err := u.generateRandomCode()
		if err != nil {
			return nil, err
		}
		codes = append(codes, code)
	}
	return codes, nil
}

func (u *User) generateRandomCode() (string, error) {
	//goland:noinspection SpellCheckingInspection
	const characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	ret := make([]byte, 10)
	for i := 0; i < 10; i++ {
		num, err := rand.Int(rand.Reader, big.NewInt(int64(len(characters))))
		if err != nil {
			return "", err
		}
		ret[i] = characters[num.Int64()]
	}
	return string(ret), nil
}

func (u *User) hashPassword(password string) (string, error) {
	cost := configuration.Config.PasswordHashCost
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	return string(bytes), err
}

func (u *User) inlineHashPassword(password string) string {
	hashed, _ := u.hashPassword(password)
	return hashed
}
