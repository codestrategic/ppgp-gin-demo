package models

import "time"

type GoalData struct {
	GoalCount     int       `json:"goal_count"`
	RetrievalDate time.Time `json:"retrieval_date"`
}
