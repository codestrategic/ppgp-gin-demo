package main

import (
	"codestrategic.com/ppgp/handlers"
	"codestrategic.com/ppgp/middleware"
)

func InitializeRoutes(api *handlers.APIEnv) {

	// Set the is_logged_in status properly on all routes
	router.Use(middleware.SetUserStatus())

	router.Static("/assets", "./assets")

	// View home page
	router.GET("/", api.ShowHomePage)

	// Group the admin-related routes under '/admin'
	AdminRoutes := router.Group("/admin")
	{
		AdminRoutes.GET("", middleware.EnsureLoggedIn(), api.ShowDashboard)
	}

	AuthRoutes := router.Group("/auth")
	{
		// Login page
		AuthRoutes.GET("/login", api.ShowLoginPage)

		// Submit login
		AuthRoutes.POST("/login", api.PerformLogin)

		// Validate with 2fa page
		AuthRoutes.GET("/validate2fa", middleware.EnsureLoginFormSucceeded(), api.Show2faValidationPage)

		// Submit 2fa verification code
		AuthRoutes.POST("/validate2fa", middleware.EnsureLoginFormSucceeded(), api.Perform2faValidation)

		// Validate with recovery code page
		AuthRoutes.GET("/validateRecoveryCode", middleware.EnsureLoginFormSucceeded(), api.ShowRecoveryCodeValidationPage)

		// Submit recovery code
		AuthRoutes.POST("/validateRecoveryCode", middleware.EnsureLoginFormSucceeded(), api.PerformRecoveryCodeValidation)

		// Logout
		AuthRoutes.GET("/logout", api.Logout)

	}

	TwoFaRoutes := router.Group("/user/2fa")
	{
		TwoFaRoutes.GET("/status", middleware.EnsureLoggedIn(), api.Status2fa)

		TwoFaRoutes.POST("/enable", middleware.EnsureLoggedIn(), api.Enable2fa)

		TwoFaRoutes.GET("/configure", middleware.EnsureLoggedIn(), api.Configure2fa)

		TwoFaRoutes.POST("/confirm", middleware.EnsureLoggedIn(), api.Confirm2fa)

		TwoFaRoutes.POST("/disable", middleware.EnsureLoggedIn(), api.Disable2fa)
	}

	UserRoutes := router.Group("/user")
	{
		UserRoutes.GET("", middleware.EnsureLoggedIn(), api.ShowUsers)

		UserRoutes.GET("/view/:id", api.ViewUser)

		UserRoutes.GET("/edit/:id", api.EditUser)

		UserRoutes.POST("/edit/:id", api.UpdateUser)

		UserRoutes.GET("/my_profile", middleware.EnsureLoggedIn(), api.EditMyProfile)

		UserRoutes.POST("/my_profile", middleware.EnsureLoggedIn(), api.UpdateMyProfile)

	}

}
